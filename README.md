ics-ans-role-cs-studio
======================

Ansible role to install CS-Studio.

Role Variables
--------------

```yaml
csstudio_version: 4.6.1.25
csstudio_repository: production
csstudio_base_url: "https://artifactory.esss.lu.se/artifactory/CS-Studio"
csstudio_archive: "{{csstudio_base_url}}/{{csstudio_repository}}/{{csstudio_version}}/cs-studio-ess-{{csstudio_version}}-linux.gtk.x86_64.tar.gz"
# set to true to remove previous installations (keep only current version)
csstudio_clean_previous_install: true
# set to true to remove existing users preferences (to make sure the installed ones are used)
csstudio_reset_users_preferences: true
# When changing the fonts, don't forget to remove
# the previous directories!
csstudio_fonts: https://artifactory.esss.lu.se/artifactory/swi-pkg/fonts/cs-studio-fonts-20180222.tgz
csstudio_fonts_dir: /usr/share/fonts/ess-20180222
# xulrunner downloaded from http://releases.mozilla.org/pub/xulrunner/nightly/2012/05/2012-05-13-03-32-04-mozilla-1.9.2/
# x86_64 version only available in nightly (not in releases)
csstudio_xulrunner_version: 1.9.2.29pre
csstudio_xulrunner: https://artifactory.esss.lu.se/artifactory/swi-pkg/xulrunner/xulrunner-{{csstudio_xulrunner_version}}.en-US.linux-x86_64.tar.bz2
# Set to [] to not create a python environment
csstudio_python_packages:
  - python=3.7
  - py4j
  - connect2j
  - numpy=1.17
  - scipy=1.3
  - pandas=0.25
# Java heap size
csstudio_java_max_heap_size: 2g

# ESS CS-Studio git folders version to mount
csstudio_ess_templates_version: 1.0.4
csstudio_ess_symbols_version: 1.0.5
csstudio_ess_opis_version: master

# List of .opi that must be opened by BOY even if the action is triggered
# by Display Builder. To specify all .opi files, set to ".opi" (default).
csstudio_display_builder_use_boy: ".opi"
# Update delay for display builder
csstudio_display_builder_update_delay: 120
csstudio_display_builder_plot_update_delay: 120
csstudio_display_builder_image_update_delay: 120

# If set to true, ignore *.plt files
csstudio_trends_use_default_archives: "true"
# Default request type for newly created trace/PVItem
# Valid values are: RAW and OPTIMIZED
csstudio_trends_request_type: RAW

# Channel Access preferences
# space separated list of addresses
csstudio_ca_addr_list: localhost
csstudio_ca_auto_addr_list: "true"
csstudio_ca_max_array_size: "10000000"
csstudio_ca_dbe_property_supported: "true"

# root components defined in beast playbook
csstudio_beast_root_component: MAIN
csstudio_annunciator_jms_topic: ALARM
# Following variables should be identical to the ones set in the beast playbook
# Don't forget to add "?serverTimezone=UTC" at the end of the URL
beast_rdb_url: jdbc:mysql://localhost/alarm?serverTimezone=UTC
beast_rdb_user: alarm
beast_rdb_password: alarmpwd
beast_jms_url: failover:(tcp://localhost:61616)
beast_jms_user: admin
beast_jms_password: admin
# Message history configuration
# User and password shall come from the AWX jms2rdb credentials
csstudio_msghist_rdb_url: jdbc:postgresql://localhost/log
jms2rdb_postgres_ro_user: report
jms2rdb_postgres_ro_password: secret
```

Example Playbook
----------------

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-cs-studio
```

License
-------

BSD 2-clause
