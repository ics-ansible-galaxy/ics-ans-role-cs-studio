import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('clean_previous_false')


def test_previous_install_not_removed(host):
    assert host.file('/opt/cs-studio-production-4.5.6.0').exists
