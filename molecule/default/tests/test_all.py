import os
import re
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')

CSSTUDIO_VERSION = '4.6.1.25'


def test_csstudio_symlink(host):
    csstudio_home = '/opt/cs-studio-production-{}'.format(CSSTUDIO_VERSION)
    opt_cs_studio = host.file('/opt/cs-studio')
    assert opt_cs_studio.is_symlink
    assert opt_cs_studio.linked_to == csstudio_home
    assert host.file(csstudio_home).is_directory


def test_csstudio_version(host):
    version = host.file('/opt/cs-studio/ess-version.txt').content_string
    assert version.strip() == CSSTUDIO_VERSION


def test_csstudio_startup_script(host):
    script = host.file('/usr/local/bin/css')
    assert script.is_file
    assert r"/opt/cs-studio/ESS\ CS-Studio $@ >/dev/null 2>&1" in script.content_string


def test_csstudio_binary(host):
    assert host.file('/opt/cs-studio/ESS CS-Studio').exists


def test_fonts(host):
    cmd = host.run('fc-match -s "monospace"')
    assert cmd.stdout.startswith('NimbusMonoPS-Regular.otf: "Nimbus Mono PS" "Regular"')
    assert 'Source Sans Pro' in cmd.stdout
    assert 'Titillium' in cmd.stdout
    cmd = host.run('fc-list')
    assert 'Open Sans' in cmd.stdout
    assert 'Roboto' in cmd.stdout


def test_xulrunner(host):
    cmd = host.run('/opt/xulrunner/xulrunner -v 2>&1')
    assert cmd.stdout.strip() == 'Mozilla XULRunner 1.9.2.29pre - 20120513033204'


def test_eclipse_directory_removed(host):
    assert not host.file('/home/user/.eclipse').exists


def test_git_folders_project_exists(host):
    assert host.file('/usr/local/share/cs-studio/ess-templates/.project').exists
    assert host.file('/usr/local/share/cs-studio/ess-symbols/.project').exists
    assert host.file('/usr/local/share/cs-studio/ess-opis/.project').exists


def test_csstudio_share_link_options(host):
    content = re.compile(r"""\#\sBEGIN\sANSIBLE\sMANAGED\sBLOCK\n
                         \-share_link\n
                         /usr/local/share/cs-studio/ess-templates=/ess-templates,/usr/local/share/cs-studio/ess-symbols=/ess-symbols,/usr/local/share/cs-studio/ess-opis=/ess-opis\n
                         \#\sEND\sANSIBLE\sMANAGED\sBLOCK\n
                         \-\-launcher.GTK_version""", re.MULTILINE | re.VERBOSE)
    csstudio_ini = host.file('/opt/cs-studio/ESS CS-Studio.ini')
    assert csstudio_ini.exists
    assert content.search(csstudio_ini.content_string) is not None


def test_csstudio_configuration(host):
    config = host.file('/opt/cs-studio/configuration/plugin_customization.ini')
    assert config.contains('org.csstudio.alarm.beast.msghist/rdb_schema=public')
    assert config.contains('org.csstudio.alarm.beast.msghist/rdb_url=jdbc:postgresql://localhost/log')
    assert config.contains('org.csstudio.alarm.beast/rdb_url=jdbc:mysql://localhost/alarm?serverTimezone=UTC')
    assert config.contains('org.csstudio.display.builder.runtime/use_boy=.opi')


def test_csstudio_java_heap_size(host):
    csstudio_ini = host.file('/opt/cs-studio/ESS CS-Studio.ini')
    assert not csstudio_ini.contains("-Xms")
    if "no_python_env" in host.ansible.get_variables()['group_names']:
        assert csstudio_ini.contains("-Xmx16g")
    else:
        assert csstudio_ini.contains("-Xmx2g")
