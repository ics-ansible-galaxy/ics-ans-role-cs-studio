import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('no_python_env')


def test_no_conda_env_installed(host):
    assert not host.file('/opt/conda/envs/csstudio').exists
