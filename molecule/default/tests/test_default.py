import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ["MOLECULE_INVENTORY_FILE"]
).get_hosts("default_group")


def test_conda_env_csstudio_exists(host):
    assert host.file("/opt/conda/envs/csstudio/bin").exists


def test_import_connect2j(host):
    cmd = host.run('/opt/conda/envs/csstudio/bin/python -c "import connect2j"')
    assert cmd.rc == 0


def test_previous_install_removed(host):
    assert not host.file("/opt/cs-studio-production-4.5.6.0").exists


def test_plugin_customization(host):
    plugin_customization = host.file(
        "/opt/cs-studio/configuration/plugin_customization.ini"
    )
    assert plugin_customization.exists
    assert plugin_customization.contains(
        "org.csstudio.trends.databrowser3/use_default_archives=true"
    )
    assert plugin_customization.contains(
        "org.csstudio.trends.databrowser3/request_type=RAW"
    )
    assert plugin_customization.contains(
        "org.csstudio.diirt.util.core.preferences/diirt.ca.max.array.size=10000000"
    )
    assert plugin_customization.contains(
        "org.csstudio.display.builder.representation/update_delay=120"
    )
    if host.ansible.get_variables()["inventory_hostname"] == "ics-ans-role-cs-studio-clean-previous-false":
        assert plugin_customization.contains(
            "org.csstudio.diirt.util.core.preferences/diirt.ca.addr.list=192.168.1.75 192.168.2.42"
        )
        assert plugin_customization.contains(
            "org.csstudio.diirt.util.core.preferences/diirt.ca.auto.addr.list=false"
        )
    else:
        assert plugin_customization.contains(
            "org.csstudio.diirt.util.core.preferences/diirt.ca.addr.list=localhost"
        )
        assert plugin_customization.contains(
            "org.csstudio.diirt.util.core.preferences/diirt.ca.auto.addr.list=true"
        )


def test_users_preferences(host):
    for basedir in (
        "/home/user/.ess-cs-studio/.metadata/.plugins/org.eclipse.core.runtime/.settings",
        "/home/another/CSS/myproject/one/.metadata/.plugins/org.eclipse.core.runtime/.settings",
    ):
        for prefs in (
            "org.csstudio.alarm.beast.prefs",
            "org.csstudio.alarm.beast.annunciator.prefs",
            "org.csstudio.alarm.beast.msghist.prefs",
            "org.csstudio.diirt.util.core.preferences.prefs",
            "org.csstudio.trends.databrowser3.prefs",
        ):
            filename = os.path.join(basedir, prefs)
            if host.ansible.get_variables()["inventory_hostname"] == "ics-ans-role-cs-studio-clean-previous-false":
                assert host.file(filename).exists
            else:
                assert not host.file(filename).exists
