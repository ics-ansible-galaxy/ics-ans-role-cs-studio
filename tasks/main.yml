---
# gtk2, alsa-lib and libXt required by xulrunner
- name: install required dependencies
  yum:
    name:
      - bzip2
      - git
      - gtk2
      - alsa-lib
      - libXt
      # install fonts that have precedence on others to avoid kerning issues
      - urw-fonts
    state: present

- name: look for existing users home
  find:
    paths: /home
    file_type: directory
  register: users_home

- name: check if /opt/cs-studio is a directory
  stat:
    path: /opt/cs-studio
  register: opt_cs_studio

- name: remove old /opt/cs-studio directory
  file:
    path: /opt/cs-studio
    state: absent
  when: opt_cs_studio.stat.isdir is defined and opt_cs_studio.stat.isdir

- name: check current installations
  find:
    paths: /opt
    patterns: 'cs-studio-*'
    file_type: directory
  register: find_csstudio
  tags: clean-cs-studio

- name: remove previous installations
  file:
    path: "{{ item.path }}"
    state: absent
  with_items:
    - "{{ find_csstudio.files }}"
  when:
    - csstudio_clean_previous_install
    - item.path != csstudio_home
  tags: clean-cs-studio

- name: create CS-Studio home
  file:
    path: "{{ csstudio_home }}"
    state: directory
    owner: root
    group: root
    mode: 0755

# Remove the prefix "cs-studio/" to install
# it in the desired directory
- name: install CS-Studio
  unarchive:
    src: "{{ csstudio_archive }}"
    dest: "{{ csstudio_home }}"
    extra_opts:
      - --transform
      - "s?^cs-studio/??"
    remote_src: true
    owner: root
    group: root
    creates: "{{ csstudio_home }}/ESS CS-Studio"
  notify:
    - remove .eclipse directory

- name: create the /opt/cs-studio link
  file:
    src: "{{ csstudio_home }}"
    dest: /opt/cs-studio
    state: link
    follow: false
    owner: root
    group: root

- name: customize Java max heap size
  lineinfile:
    path: "/opt/cs-studio/ESS CS-Studio.ini"
    regexp: "^-Xmx"
    line: "-Xmx{{ csstudio_java_max_heap_size }}"

- name: remove Java initial heap size
  lineinfile:
    path: "/opt/cs-studio/ESS CS-Studio.ini"
    regexp: "^-Xms"
    state: absent

- name: remove old diirt configuration directory
  file:
    path: /opt/cs-studio/configuration/diirt
    state: absent

- name: customize CSS preference settings
  lineinfile:
    state: present
    path: /opt/cs-studio/configuration/plugin_customization.ini
    regexp: "{{ item.regexp }}"
    line: "{{ item.line }}"
  no_log: true
  with_items: "{{ csstudio_preferences }}"
  notify:
    - reset users preferences

- name: copy the /usr/local/bin/css script
  copy:
    src: css
    dest: /usr/local/bin/css
    owner: root
    group: root
    mode: 0755
  tags:
    - css-script

- name: create the /ess directory
  file:
    path: /ess
    state: directory
    owner: root
    group: root
    mode: 0755

- name: add CS-Studio to the desktop menu
  copy:
    src: ESS-CS-Studio.desktop
    dest: /usr/share/applications
    owner: root
    group: root
    mode: 0644

- name: install xulrunner
  unarchive:
    src: "{{ csstudio_xulrunner }}"
    dest: /opt
    remote_src: true
    owner: root
    group: root

- name: create the /ess/xulrunner link for backward compatibility
  file:
    src: /opt/xulrunner
    dest: /ess/xulrunner
    state: link
    owner: root
    group: root

- name: create the CS-Studio fonts directory
  file:
    path: "{{ csstudio_fonts_dir }}"
    state: directory
    owner: root
    group: root
    mode: 0755

# When changing the fonts, don't forget to remove
# the previous directories!
- name: remove old CS-Studio fonts
  file:
    path: "/usr/share/fonts/{{ item }}"
    state: absent
  with_items:
    - ess

- name: install CS-Studio fonts
  unarchive:
    src: "{{ csstudio_fonts }}"
    dest: "{{ csstudio_fonts_dir }}"
    remote_src: true
    owner: root
    group: root


- name: remove old csstudio_env.yml environment file
  file:
    path: /opt/conda/csstudio_env.yml
    state: absent
  register: csstudio_env_file
  tags: csstudio-python-env

# The old conda env created from the environment file
# can't be upgraded using the conda module due to:
# specifications were found to be incompatible with a past
# explicit spec that is not an explicit spec
- name: remove old csstudio conda environment
  conda_env:
    name: csstudio
    state: absent
  when: csstudio_env_file.changed  # noqa 503
  tags: csstudio-python-env

- block:
    - name: install Python environment
      conda:
        name: "{{ csstudio_python_packages }}"
        state: present
        channels:
          - ics-conda
          - conda-forge
          - anaconda-main
        environment: csstudio
  rescue:
    - name: remove csstudio environment (update failed)
      conda_env:
        name: csstudio
        state: absent
    - name: create Python environment
      conda:
        name: "{{ csstudio_python_packages }}"
        state: present
        channels:
          - ics-conda
          - conda-forge
          - anaconda-main
        environment: csstudio
  when: csstudio_python_packages != []
  tags: csstudio-python-env

# Do not use a loop to be able to trigger an individual
# update of the git repository by passing only one version
- name: install CS-Studio ess-templates folder
  git:
    repo: https://gitlab.esss.lu.se/ics-software/ess-templates.git
    dest: "{{ csstudio_git_folders_base }}/ess-templates"
    version: "{{ csstudio_ess_templates_version }}"
  tags:
    - csstudio-ess-templates
    - csstudio-git-folders

- name: install CS-Studio ess-symbols folder
  git:
    repo: https://gitlab.esss.lu.se/ics-software/ess-symbols.git
    dest: "{{ csstudio_git_folders_base }}/ess-symbols"
    version: "{{ csstudio_ess_symbols_version }}"
  tags:
    - csstudio-ess-symbols
    - csstudio-git-folders

- name: install CS-Studio ess-opis folder
  git:
    repo: https://gitlab.esss.lu.se/ics-software/ess-opis.git
    dest: "{{ csstudio_git_folders_base }}/ess-opis"
    version: "{{ csstudio_ess_opis_version }}"
  tags:
    - csstudio-ess-opis
    - csstudio-git-folders

# WARNING: both blocks should use different markers!
- name: add Java 8 path to ESS CS-Studio.ini
  blockinfile:
    path: "/opt/cs-studio/ESS CS-Studio.ini"
    insertbefore: "^-vmargs"
    marker_begin: BEGIN VM
    marker_end: END VM
    block: |
      -vm
      {{ java_oracle_jdk8_home }}/jre/bin

- name: customize ESS CS-Studio.ini to mount git folders
  blockinfile:
    path: "/opt/cs-studio/ESS CS-Studio.ini"
    insertbefore: "^--launcher.GTK_version"
    block: |
      -share_link
      {{ csstudio_git_folders_base }}/ess-templates=/ess-templates,{{ csstudio_git_folders_base }}/ess-symbols=/ess-symbols,{{ csstudio_git_folders_base }}/ess-opis=/ess-opis
  tags:
    - csstudio-ess-templates
    - csstudio-ess-symbols
    - csstudio-ess-opis
    - csstudio-git-folders
